class
	GAME
create
	make
feature

	rolls : ARRAY [INTEGER]
	current_roll : INTEGER

	make
		do
			current_roll := 1
			create rolls.make_filled (0, 1, 21)
		end

	roll(pins : INTEGER)
		do
			rolls[current_roll] := pins
			current_roll := current_roll + 1
		end

	score : INTEGER
		local
			sc : INTEGER
			frame_index : INTEGER
			frame : INTEGER
		do
			frame_index := 1

			from
				frame := 1
			until
				frame > 10
			loop
				if is_strike(frame_index)then
					sc := sc + 10 + strike_bonus(frame_index)
					frame_index := frame_index + 1
				elseif is_spare(frame_index) then
					sc := sc + 10 + spare_bonus(frame_index)
					frame_index := frame_index + 2
				else
					sc := sc + sum_of_balls_in_frame(frame_index);
					frame_index := frame_index + 2;
				end
				frame := frame + 1
			end

			Result := sc
		end

	is_strike(fr : INTEGER) : BOOLEAN
		do
			if rolls[fr] = 10 then
				Result := true
			else
				Result := false
			end
		end

	sum_of_balls_in_frame(fr : INTEGER) : INTEGER
		do
			Result := rolls[fr] + rolls[fr + 1]
		end

	spare_bonus(fr : INTEGER) : INTEGER
		do
			Result := rolls[fr + 2]
		end

	strike_bonus(fr : INTEGER) : INTEGER
		do
			Result := rolls[fr + 1] + rolls[fr + 2]
		end

	is_spare(fr : INTEGER) : BOOLEAN
		do
			if rolls[fr] + rolls[fr + 1] = 10 then
				Result := true
			else
				Result := false
			end
		end
end
