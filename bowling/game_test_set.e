note
	description: "[
		Eiffel tests that can be executed by testing tool.
	]"
	author: "EiffelStudio test wizard"
	date: "$Date$"
	revision: "$Revision$"
	testing: "type/manual"

class
	GAME_TEST_SET

inherit
	EQA_TEST_SET
		redefine
			on_prepare
		end

feature {NONE} -- Events

	g : GAME

	on_prepare
			-- <Precursor>
		do
			create g.make
		end

    roll_many (n : INTEGER; pins : INTEGER)
    	local
    		i : INTEGER
    	do
    		from
    			i := 1
    		until
    			i >= n
    		loop
    			g.roll(pins)
    			i := i + 1
    		end
    	end


feature -- Test routines

	test_gutter_game
		do
			roll_many(20, 0)
        	assert ("Score is not zero: " + g.score.out, g.score = 0)
		end

	test_all_ones
		do
			roll_many (20, 1)
			assert ("Score is not twenty: " + g.score.out, g.score = 20)
		end


end


