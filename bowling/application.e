note
	description: "bowling application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS_32

create
	make

feature {NONE} -- Initialization
	g : GAME

	make
		local
    		i : INTEGER
    		pins : INTEGER
    		n : INTEGER
    	do
    		create g.make
    		--print("Score is not zero: " + g.score.out)
    		pins := 4
    		n := 20
    		from
    			i := 1
    		until
    			i >= n
    		loop
    			g.roll(pins)
    			i := i + 1
    		end

    		print(g.score.out)
    	end

end
